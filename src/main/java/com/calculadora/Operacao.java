package com.calculadora;

import java.util.List;

public class Operacao {

    public static int soma(int numeroUm, int numeroDois){
        return numeroUm + numeroDois;
    }

    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero : numeros){
            resultado += numero;
        }
        return resultado;
    }

    public static int subtracao(int numeroUm, int numeroDois){
        return numeroUm - numeroDois;
    }

    public static int subtracao(List<Integer> numeros){
        int resultado = numeros.get(0);
        for (int i = 1; i < numeros.size(); i++){
            resultado -= numeros.get(i);
        }
        return resultado;
    }

    public static int multiplicacao(int numeroUm, int numeroDois){
        return numeroUm * numeroDois;
    }

    public static int multiplicacao(List<Integer> numeros){
        int resultado = numeros.get(0);
        for(int i = 1; i < numeros.size(); i++){
            resultado *= numeros.get(i);
        }
        return resultado;
    }

    public static int divisao(int numeroUm, int numeroDois){
        return numeroUm/numeroDois;
    }
}
