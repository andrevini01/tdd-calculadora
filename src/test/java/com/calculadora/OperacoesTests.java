package com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(Operacao.soma(numeros), 6);
    }

    @Test
    public void testarOperacaoDeSomaDeDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,1), 2);
    }

    @Test
    public void testarOperacaoDeSubtracaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.subtracao(2,1),1);
    }

    @Test
    public void testarOperacaoDeSubtracao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(4);
        numeros.add(3);
        numeros.add(2);
        Assertions.assertEquals(Operacao.subtracao(numeros), -1);
    }

    @Test
    public void testarOperacaoDeMultiplicacaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(2,2), 4);
    }

    @Test
    public void testarOperacaoDeMultiplicacao(){
        List<Integer> numeros = new ArrayList<>();
        numeros.add(4);
        numeros.add(2);
        numeros.add(2);
        Assertions.assertEquals(Operacao.multiplicacao(numeros), 16);
    }

    @Test
    public void testarOperacaoDeDivisaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.divisao(4,2), 2);
    }

    @Test
    public void testarOperacaoDeDivisaoValorInvalido(){
        Assertions.assertThrows(ArithmeticException.class, () -> {
                Operacao.divisao(1,0);
        });
    }
}
